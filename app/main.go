package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
)

type Year struct {
	Year   string  `json:"year"`
	Months []Month `json:"months"`
}

type Month struct {
	Name  string `json:"month"`
	Weeks []Week `json:"weeks"`
}

type Week struct {
	NotAvailable bool `json:"status"`
}

type Resource struct {
	Name string `json:"name"`
	Role string `json:"role"`
}

type Resources struct {
	Role  string `json:"role"`
	Count int    `json:"count"`
}

type Project struct {
	ID        string      `json:"id"`
	Resources []Resources `json:"resources"`
	Duration  int         `json:"duration"`
}

func main() {

	resourcesData, err := ioutil.ReadFile("data/resources.json")
	if err != nil {
		fmt.Println(err)
	}

	projectData, err := ioutil.ReadFile("data/projects.json")
	if err != nil {
		fmt.Println(err)
	}

	dateData, err := ioutil.ReadFile("data/months.json")
	if err != nil {
		fmt.Println(err)
	}

	var resources []Resource
	var projects []Project
	var year Year

	err = json.Unmarshal(resourcesData, &resources)
	fmt.Println(resources)

	err = json.Unmarshal(projectData, &projects)
	fmt.Println(projects)

	err = json.Unmarshal(dateData, &year)
	//fmt.Println(year.Months[0].Weeks[1])
	//fmt.Println(year.Months[0].Name)

	// for _, v := range year.Months {
	// 	fmt.Println(v.Name)
	// }

	for _, v := range projects {

		count := 0
		if checkSkillsExist(v.Resources, resources) {
			fmt.Printf("%s Skills Exist.\n", v.ID)
			for count <= v.Duration {
				month := 0
				weeklyCount := 0

				year.Months[count].Weeks[weeklyCount].NotAvailable = true
				fmt.Println(year.Months[month].Name)
				fmt.Println(year.Months[month].Weeks[weeklyCount].NotAvailable)

				if count%4 == 0 {
					month = month + 1
					weeklyCount = 0

				}
				//dateAvailability[position] = true
				count = count + 1
				weeklyCount = weeklyCount + 1
			}
		} else {
			fmt.Printf("%s not feasible: no skills\n", v.ID)
		}
	}
	fmt.Println(year)

	//Must have one QA and 1 Ops
	//if cant be finished by dec dont start

}

func checkSkillsExist(resources []Resources, availableResources []Resource) bool {
	var available []string

	for _, v := range availableResources {
		available = append(available, v.Role)
	}

	for _, v := range resources {
		exists, _ := inArray(v.Role, available)

		if exists {
			return true
		} else {
			return false
		}
	}
	return true
}

func inArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}

	return
}
