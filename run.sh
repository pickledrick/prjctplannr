#!/usr/bin/env sh

echo "##############################################"
echo "### Building binary and creating container ###"
echo "##############################################"
GOOS=linux GOARCH=amd64 go build -o prjctplannr ./app
docker build . -t prjctplannr:app
echo "#############################################"
echo "### Running container                     ###"
echo "#############################################"
docker run -i --rm prjctplannr:app
