FROM alpine

COPY app/data /go/bin/data/
ADD prjctplannr /go/bin/

CMD cd /go/bin/ && ./prjctplannr
